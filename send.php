<?php
require_once 'lib'.DIRECTORY_SEPARATOR.'curl.php';
require_once 'lib'.DIRECTORY_SEPARATOR.'curl_response.php';
require_once 'SmsSender.php';

$config = require_once 'config.php';
$recipients = array(
	'phone number'
);
$message = 'ENTER MESSAGE HERE';

$smser = new SmsSender($config['access_token'], $recipients, $message);

$result = $smser->send();

if ($result['status'] == 'ok') {
	echo "Message sent.\n";
} else {
	echo "Failed sending message: \n";
	foreach ($result['errors'] as $error) {
		echo "ERROR: " . $error . "\n";
	}
}
