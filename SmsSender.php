<?php
/**
 * Sends SMSes through Free SMS Gateway
 * http://www.freesmsgateway.com/
 */

/**
 * Class SmsSender sends SMS messages to phones through freesmsgateway.com gateway
 */
class SmsSender {
	private $message;
	private $recipients;
	private $accessToken;
	private $errors;

	function __construct($accessToken = '', $recipients = array(), $message = '')
	{
		$this->accessToken = $accessToken;
		$this->recipients  = $recipients;
		$this->message     = $message;
	}

	public function setAccessToken($token)
	{
		$this->accessToken = $token;
	}

	public function addRecipients($recipients = array())
	{
		$this->recipients = $recipients;
	}

	public function setMessage($message = '')
	{
		$this->message = $message;
	}

	private function preCheck()
	{
		$errors = array();
		if (empty($this->accessToken)) {
			$errors[] = 'No access token specified.';
		}
		if (empty($this->recipients)) {
			$errors[] = 'There is no any recipient to send message to.';
		}
		if (empty($this->message)) {
			$errors[] = 'The message is empty.';
		}
		$this->errors = $errors;
		if (empty($errors)) {
			return true;
		} else {
			return false;
		}
	}

	public function send()
	{
		if ($this->preCheck()) {
			$curl = new Curl();
			$postFields = array(
				'access_token' => $this->accessToken,
				'message' => urlencode($this->message),
				'send_to' => 'post_contacts',
				'post_contacts' => urlencode(json_encode($this->recipients)),
			);
			$postVars = '';
			foreach ($postFields as $key => $value) {
				$postVars .= $key . '=' . $value . '&';
			}
			rtrim($postVars, '&');
			$response = $curl->post('http://www.freesmsgateway.com/api_send', $postVars);

			file_put_contents('response.html', $response->body, FILE_APPEND);
			var_dump($response);

			return array('status' => 'ok');
		} else {
			return array('status' => 'error', 'errors' => $this->errors);
		}
	}

}
